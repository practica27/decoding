use std::process::Command;
use std::convert::TryInto;
use std::fs;

fn position_in_alphabet(alphabet:&mut [char], character:char) -> u32{
    
    let mut position_in_alphabet = 0;
 
    let mut c = 0;

    loop {
        
        if c == alphabet.len(){
            break;
        }
        if alphabet[c] == character{
            position_in_alphabet = c as u32;
          break;
            
        }

        c = c+1;

    }   
    
   return position_in_alphabet;

}


fn decoding(){
    let mut alphabet = [
        'a','b','c','d','e','f','g',
        'h','i','j','k','l','m','n',
        'o','p','q','r','s','t','u',
        'v','w','x','y','z','A','B',
        'C','D','E','F','G','H','I',
        'J','K','L','M','N','O','P',
        'Q','R','S','T','U','V','W',
        'X','Y','Z','0','1','2','3',
        '4','5','6','7','8','9'    
        ];


        let mut file_content = fs::read_to_string("data.txt");

        let mut secret_word:String = (&"").to_string();
        let mut encrypted_messages : Vec<String> = Vec::new();  

        let mut file_content_list : Vec<String> = Vec::new();  
        let mut file_line:String = (&"").to_string();

        let mut a = 0;
        loop {

            

            if a == file_content.as_ref().expect("REASON").chars().count(){
                break;
            }

            let mut current_character = file_content.as_ref().expect("REASON").chars().nth(a).unwrap();

            if current_character != '\r' && current_character != '\n'{

                file_line = file_line.to_owned()+&current_character.to_string();
                 

            } 

            if current_character == '\n'{
                file_content_list.push((&file_line).to_string());
                file_line = (&"").to_string();
                

            }

          a = a+1;

        }
        file_content_list.push((&file_line).to_string());

        let mut a = 0;

        loop {
            
            if a == file_content_list.len(){
                break;
            }

            if a == 0{
                secret_word = file_content_list[a].to_string();

            }else{
                encrypted_messages.push((&file_content_list[a]).to_string());

            }

            a = a+1;
    
        }            

        let mut decoded_messages : Vec<String> = Vec::new();  
        let mut number_position_secret_word = 0;

        let mut a = 0;
        
        loop {
            
            if a == secret_word.chars().count(){
                break;
            }

            let mut ascii_code  = secret_word.chars().nth(a).unwrap() as u32;
            number_position_secret_word = number_position_secret_word+ascii_code;
                       
            a = a+1;
    
        } 

        let mut a = 0;


        loop {
            
            if a == encrypted_messages.len(){
                break;
            }
    
            let mut encrypted_message = encrypted_messages[a].replace(" ","");
            let mut decoded_message = "".to_string();            
    

            let mut b = 0;
            loop {
            
                if b == encrypted_message.chars().count(){
                    break;
                }
                
                let mut character_current_encrypted_message = encrypted_message.chars().nth(b).unwrap();              
                let mut quantity_current_positions = number_position_secret_word;     

                if decoded_message.chars().count()>0 {
                    quantity_current_positions = quantity_current_positions+position_in_alphabet(&mut alphabet, encrypted_message.chars().nth(b-1).unwrap());
                    
                  }    

              let mut position_character_current_message_coded_in_alphabet = position_in_alphabet(&mut alphabet, character_current_encrypted_message );

               if position_character_current_message_coded_in_alphabet>=0 {
                 
                    let mut c = 0;
                    
                    loop {

                     if c == quantity_current_positions{
                            break;
                     } 
                    
                     if position_character_current_message_coded_in_alphabet == 0 {

                        position_character_current_message_coded_in_alphabet = (alphabet.len()-1).try_into().unwrap();

                        
                        }else{
                        
                        position_character_current_message_coded_in_alphabet = position_character_current_message_coded_in_alphabet-1;
                        }                     

                     c = c+1;

                    }
                    let mut index_position:usize = position_character_current_message_coded_in_alphabet.try_into().unwrap(); 
                    decoded_message = decoded_message+&alphabet[index_position].to_string();

                 }


                b = b+1;
        
            }

            decoded_messages.push((&decoded_message).to_string());
            a = a+1;
    
        }

        let mut a = 0;

        loop {
            
            if a == decoded_messages.len(){
                break;
            }

            println!("Word: {:?}", decoded_messages[a] );           
    
            a = a+1;
    
        }         


    let _ = Command::new("cmd.exe").arg("/c").arg("pause").status();

}

fn main(){

    decoding();
}